const express = require("express");
const app = express();
const cookie = require("cookie-parser");
const cors = require("cors");
const mongoose = require("mongoose");
const morgan = require("morgan");
require('dotenv/config')


const options = {withCredentials: true, 'access-control-allow-origin': "http://localhost:8100/", 'Content-Type': 'application/json'} 


app.use(cors(options))

app.use(express.json())

app.use(morgan('dev'))
app.use(cookie())


const bookRouter = require("./Router/bookRouter")
app.use('/api/books',bookRouter)

app.listen(2000, () => {
    console.log("welcome");
})

mongoose.set('useNewUrlParser',true)
mongoose.set('useUnifiedTopology',true)

mongoose.connect(process.env.MONGDODB_CONN,(err) => {
    if(err){
        console.log("Db not connected");
    }
    else {
        console.log("Db succesfully connected");
    }
})
