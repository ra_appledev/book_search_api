const bookSchema = require('../Schema/bookSchema');
const mongoose = require('mongoose')

exports.uploadBookDetail = async (req, res, next) => {
    try {
        const postBook = await new bookSchema({
            bookName: req.body.bookName,
            authorName: req.body.authorName,
            image: req.body.image,
            detail: req.body.detail,
        })

        if (postBook) {
            const saveBook = await postBook.save();
            res.status(200).json(saveBook)
        }
    } catch (err) {
        console.log(err);
    }
}

exports.getAllBook = async (req, res, next) => {
    try {
        const getAll = await bookSchema.find()
        res.status(200).json(getAll)

    }catch (err) {
        console.log(err);
    }
}

exports.searchBook = async (req, res, next) => {
    const searchField = req.query.name
    try {
        var name = searchField.trim()[0].toUpperCase() + searchField.slice(1).toLowerCase();
        const getByBookName = await bookSchema.find({ $text: { $search: name } })
        if(getByBookName.length){
        res.status(200).json(getByBookName)
        }else {
            res.status(204).send("No data Found")
        }

    } catch (error) {
        console.log(error);
    }
}
