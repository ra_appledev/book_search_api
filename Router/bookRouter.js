const express = require('express')
const router = express.Router();
const bookSchema = require('../Schema/bookSchema');
const BookController = require('../controllers/bookControllers')

router.post('/',BookController.uploadBookDetail)

router.get('/',BookController.getAllBook);

router.post('/name',BookController.searchBook)





module.exports = router