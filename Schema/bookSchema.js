const mongoose = require('mongoose')


const bookSchema = mongoose.Schema({
    bookName: {
        type: String,
        required: true
    },
    authorName: {
        type: String,
        required: true
    },
    image: {
        type: String,
        required: true
    },
    detail: {
        type: String,
    },
    createdDate: {
        type: Date,
        default: Date.now()
    }
})

module.exports = mongoose.model('Books',bookSchema)